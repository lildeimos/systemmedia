import bb.cascades 1.3

NavigationPane {
    id: nav

    onCreationCompleted: {
        _systemMedia.finishedVideos.connect(onFinishedVideos);
        _systemMedia.gotThumbVideo.connect(onGotThumbVideo);
        
        _systemMedia.finishedImages.connect(onFinishedImages);
        _systemMedia.gotThumbImage.connect(onThumbImage);
        
        _systemMedia.finishedAudio.connect(onFinishedAudio);
        _systemMedia.gotThumbAudio.connect(onGotThumbAudio);
        
        _systemMedia.noAudioFound.connect(onNoMediaFound);
        _systemMedia.noImagesFound.connect(onNoMediaFound);
        _systemMedia.noVideosFound.connect(onNoMediaFound);
    }
    
    function onNoMediaFound() {
        console.log("No media found")
    }

    function onGotThumbVideo(list) {
        // insert thumbnails received
        listView.dataModel.insertList(list);
    }
    function onFinishedVideos() {
        console.log("*************QML onFinishedVideos FINISHED", listView.dataModel.size());
    }
    
    
    function onThumbImage(list) {
        // insert thumbnails received
        listView.dataModel.insertList(list);
    }
    function onFinishedImages() {
        console.log("*************QML onFinishedImages FINISHED", listView.dataModel.size());
    }

    
    function onGotThumbAudio(list) {
        // insert thumbnails received
        listView.dataModel.insertList(list);
    }
    function onFinishedAudio() {
        console.log("*************QML onFinishedAudio FINISHED", listView.dataModel.size());
    }
    
    Page {
        id: mainPage

        titleBar: TitleBar {
            id: titleBar
            kind: TitleBarKind.Segmented
            options: [
                Option {
                    text: "Videos"
                    value: "videos"
                },
                Option {
                    text: "Audio"
                    value: "audio"
                },
                Option {
                    text: "Images"
                    value: "images"
                }
            ]
            onSelectedIndexChanged: {
                listView.dataModel.clear();
                switch (selectedIndex) {
                    case 0: {
                            _systemMedia.terminate(); // stop thread if exists
                            _systemMedia.getVideos();
                            break;
                        }
                    case 1: {
                            _systemMedia.terminate(); // stop thread if exists
                            _systemMedia.getAudio();
                            break;
                        }
                    case 2: { 
                            _systemMedia.terminate(); // stop thread if exists
                            _systemMedia.getPictures();
                            break;
                        }
                }
            }
        }
        Container {
            ListView {
                id: listView
                property alias currentMediaListed: titleBar.selectedIndex
                dataModel: GroupDataModel {
                    sortingKeys: ["counter"]
                    grouping: ItemGrouping.None
                }
                
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        
                        Container {
                            id: itemRoot
                            
                            ////////////////////////////////////
                            // Video Container
                            Container {
                                visible: itemRoot.ListItem.view.currentMediaListed == 0 // Visible only if video is selected in TitleBar
                                // Thumbnail image
                                ImageView {
                                    maxWidth: 700
                                    maxHeight: 700
                                    imageSource: "file://" + ListItemData.thumbUrl
                                    scalingMethod: ScalingMethod.AspectFit
                                }
                                // descriptions
                                Label { text: ListItemData.counter + " - " + ListItemData.title; textStyle.fontWeight: FontWeight.Bold }
                                Label { text: "Duration: "   + ListItemData.duration + "\"" }
                                Label { text: "humanFmt: "   + ListItemData.humanFmt}
                                Label { text: "container: "  + ListItemData.container }
                                Label { text: "videoDir: "   + ListItemData.videoDir }
                                Label { text: "memorytype: " + ListItemData.memorytype }
                                Label { text: "fullVideoPath: "   + ListItemData.fullVideoPath }
                            } // video container
                            
                            
                            
                            ////////////////////////////////////
                            // Audio Container
                            Container {
                                visible: itemRoot.ListItem.view.currentMediaListed == 1 // Visible only if audio is selected in TitleBar
                                // descriptions
                                Label { text: ListItemData.counter + " - " + ListItemData.title; textStyle.fontWeight: FontWeight.Bold  }
                                Label { text: "memorytype: " + ListItemData.memorytype }
                                Label { text: "Duration: "   + (ListItemData.duration/1000) + "\"" }
                                Label { text: "bitrate: "    + ListItemData.bitrate}
                                Label { text: "year: "       + ListItemData.year }
                                Label { text: "samplerate: " + ListItemData.samplerate }
                                Label { text: "album: "      + ListItemData.album }
                                Label { text: "artist: "     + ListItemData.artist }
                                Label { text: "basepath: "   + ListItemData.basepath }
                                Label { text: "foldername: " + ListItemData.foldername }
                                Label { text: "filename: "   + ListItemData.filename }
                                Label { text: "extension: "  + ListItemData.extension }
                                Label { text: "fullAudioPath: "   + ListItemData.fullAudioPath }
                            } // audio container
                            
                            
                            
                            ////////////////////////////////////
                            // Image Container
                            Container {
                                visible: itemRoot.ListItem.view.currentMediaListed == 2 // Visible only if Images is selected in TitleBar
                                // Thumbnail image
                                ImageView {
                                    maxWidth: 700
                                    maxHeight: 700
                                    imageSource: "file://" + ListItemData.fullImagePath
                                    scalingMethod: ScalingMethod.AspectFit
                                }
                                // descriptions
                                Label { text: ListItemData.counter; textStyle.fontWeight: FontWeight.Bold }
                                Label { text: "memorytype: " + ListItemData.memorytype }
                                Label { text: "size: "   + ListItemData.width + "x" +  ListItemData.height}
                                Label { text: "orientation: "   + ListItemData.orientation }
                                Label { text: "shutter: "   + ListItemData.shutter }
                                Label { text: "aperture: "   + ListItemData.aperture }
                                Label { text: "focal_length: "   + ListItemData.focal_length }
                                Label { text: "iso: "   + ListItemData.iso }
                                Label { text: "date_original: "   + ListItemData.date_original }
                                Label { text: "date_timestamp: "   + ListItemData.date_timestamp }
                                Label { text: "latitude: "   + ListItemData.latitude }
                                Label { text: "longitude: "   + ListItemData.longitude }
                                Label { text: "basepath: "   + ListItemData.basepath }
                                Label { text: "foldername: "   + ListItemData.foldername }
                                Label { text: "filename: "   + ListItemData.filename }
                                Label { text: "extension: "   + ListItemData.extension }
                            } // image container
                            
                            
                            
                            Divider {}
                        } // itemRoot
                    } // ListItemComponent
                ]
                
            } // listView

        }

    } // mainPage
}

/*
 * SystemMedia.cpp
 *
 *  Created on: 6 Oct 2015
 *      Author: Marco Bavagnoli
 *      Copyright (c) 2015 Marco Bavagnoli <lil.deimos@gmail.com>
 */


#include "SystemMedia.h"

#include <QtGui/QTextDocument>


#define OUT_DEBUG 0

SystemMedia::SystemMedia() :
	mDb(NULL),
	mCurrentRequestMethod(QString())
{
	mDb = new dbase(this);
	mDb->setDir("/db");
	bool b;
	b = connect( mDb, SIGNAL(gotRecord(QVariantMap))  ,   this, SLOT(onGotQueryResult(QVariantMap))  );
	Q_ASSERT(b);
	b = connect( mDb, SIGNAL(gotResultsFinished())    ,   this, SLOT(onGotResultsFinished())  );
	Q_ASSERT(b);
	b = connect( mDb, SIGNAL(dbOpenError())           ,   this, SLOT(onDBOpenError())  );
    Q_ASSERT(b);
}

SystemMedia::~SystemMedia() {
    mDb->deleteLater();
}

void SystemMedia::deleteFile(QString absoluteFileName)
{
	QFile f(absoluteFileName);
	f.remove();
}

void SystemMedia::getVideos() {
	mCurrentRequestMethod = "videos";
	mCurrentMemory = "internal";
	mCounter = 0;
	start();
}

void SystemMedia::getPictures() {
	mCurrentRequestMethod = "pictures";
	mCurrentMemory = "internal";
	mCounter = 0;
	start();
}

void SystemMedia::getAudio() {
    mCurrentRequestMethod = "audio";
    mCurrentMemory = "internal";
    mCounter = 0;
    start();
}

void SystemMedia::onDBOpenError() {
    if (mCurrentRequestMethod == "audio"    && mCounter==0)  emit noAudioFound();
    if (mCurrentRequestMethod == "pictures" && mCounter==0)  emit noImagesFound();
    if (mCurrentRequestMethod == "videos"   && mCounter==0)  emit noVideosFound();
}


void SystemMedia::onGotQueryResult(const QVariantMap rec) {

	if (mCurrentRequestMethod == "videos") {
		QVariantMap video;
		QTextDocument document;
		document.setHtml(rec.value("title").toString());
		QString title = document.toPlainText();
		QString memoryText;
		QString folder;

		if (mCurrentMemory == "external") {
			memoryText = "external memory";
			folder = "/sdcard/external_sd";
		}
		if (mCurrentMemory == "internal") {
			memoryText = "device memory";
			folder = "/accounts/1000/shared";
		}
		mCounter++;
		if (OUT_DEBUG) qDebug() << "found video:" << rec;
		video.insert("counter", mCounter);
		video.insert("title", title);
		video.insert("thumbUrl", rec.value("thumbImage").toString());

		video.insert("fullVideoPath", folder + rec.value("basepath").toString() + rec.value("filename").toString());
		video.insert("memorytype", memoryText);
		video.insert("videoDir", rec.value("foldername").toString());
		video.insert("duration", QString::number(rec.value("duration").toInt()/1000));
		video.insert("humanFmt", rec.value("width").toString() + "x" + rec.value("height").toString());
		video.insert("container",  rec.value("extension").toString());

		emit gotThumbVideo(QVariantList() << video);
	}


	if (mCurrentRequestMethod == "pictures") {
			QVariantMap pictures;
			QString memoryText;
			QString folder;

			if (mCurrentMemory == "external") {
				memoryText = "external memory";
				folder = "/sdcard/external_sd";
			}
			if (mCurrentMemory == "internal") {
				memoryText = "device memory";
				folder = "/accounts/1000/shared";
			}
			mCounter++;
			if (OUT_DEBUG) qDebug() << "found picture:" << rec;
			pictures.insert("counter", 			mCounter);
			pictures.insert("memorytype", 		memoryText);
			pictures.insert("width",      		rec.value("width").toInt() );
			pictures.insert("height",     		rec.value("height").toInt() );
			pictures.insert("orientation",      rec.value("orientation").toInt() );
			pictures.insert("shutter",     		rec.value("shutter").toString() );
			pictures.insert("aperture",     	rec.value("aperture").toString() );
			pictures.insert("focal_length",     rec.value("focal_length").toInt() );
			pictures.insert("iso",     			rec.value("iso").toInt() );
			pictures.insert("date_original",    rec.value("date_original").toString() );
			pictures.insert("date_timestamp",   rec.value("date_timestamp").toDateTime() );
			pictures.insert("latitude",     	rec.value("latitude").toDouble() );
			pictures.insert("longitude",     	rec.value("longitude").toDouble() );
			pictures.insert("basepath",     	rec.value("basepath").toString() );
			pictures.insert("foldername",     	rec.value("foldername").toString() );
			pictures.insert("filename",     	rec.value("filename").toString() );
			pictures.insert("extension",     	rec.value("extension").toString() );
			pictures.insert("fullImagePath",  	folder + rec.value("basepath").toString() + rec.value("filename").toString() );

			emit gotThumbImage(QVariantList() << pictures);
		}


        if (mCurrentRequestMethod == "audio") {
            QVariantMap audio;
            QString memoryText;
            QString folder;

            if (mCurrentMemory == "external") {
                memoryText = "external memory";
                folder = "/sdcard/external_sd";
            }
            if (mCurrentMemory == "internal") {
                memoryText = "device memory";
                folder = "/accounts/1000/shared";
            }
            mCounter++;
            if (OUT_DEBUG) qDebug() << "found audio:" << rec;
            audio.insert("counter",         mCounter);
            audio.insert("memorytype",      memoryText);
            audio.insert("title",           rec.value("title").toString() );
            audio.insert("duration",        rec.value("duration").toInt() );
            audio.insert("bitrate",         rec.value("bitrate").toInt() );
            audio.insert("year",            rec.value("year").toString() );
            audio.insert("samplerate",      rec.value("samplerate").toInt() );
            audio.insert("album",           rec.value("album").toString() );
            audio.insert("artist",          rec.value("artist").toString() );
            audio.insert("basepath",        rec.value("basepath").toString() );
            audio.insert("foldername",      rec.value("foldername").toString() );
            audio.insert("filename",        rec.value("filename").toString() );
            audio.insert("extension",       rec.value("extension").toString() );
            audio.insert("fullAudioPath",   folder + rec.value("basepath").toString() + rec.value("filename").toString() );

            emit gotThumbAudio(QVariantList() << audio);
        }

}

void SystemMedia::onGotResultsFinished() {
	if (mCurrentRequestMethod == "videos") {
		if (mCurrentMemory == "internal" && QFile::exists("/db/mmlibrary_SD.db")) { // go ahead with external db
			mCurrentMemory = "external";
			start();
			return;
		}
	}

	if (mCurrentRequestMethod == "pictures") {
		if (mCurrentMemory == "internal" && QFile::exists("/db/mmlibrary_SD.db")) { // go ahead with external db
			mCurrentMemory = "external";
			start();
			return;
		}
	}

    if (mCurrentRequestMethod == "audio") {
        if (mCurrentMemory == "internal" && QFile::exists("/db/mmlibrary_SD.db")) { // go ahead with external db
            mCurrentMemory = "external";
            start();
            return;
        }
    }

    if (mCurrentRequestMethod == "videos")   { emit finishedVideos(); return; }
    if (mCurrentRequestMethod == "pictures") { emit finishedImages(); return; }
    if (mCurrentRequestMethod == "audio")    { emit finishedAudio();  return; }
}

void SystemMedia::run() {

	if (mCurrentRequestMethod == "videos") {
		if ( mCurrentMemory == "internal")
			mDb->initDB("/db/mmlibrary.db");
		else
			mDb->initDB("/db/mmlibrary_SD.db");
		QString query;
		query =
		"SELECT DISTINCT " \
		"video_metadata.width AS width, " \
		"video_metadata.height AS height, " \
		"video_metadata.duration AS duration, " \
		"video_metadata.title AS title, " \
		"video_artworks.imgfs_filename AS thumbImage, " \
		"folders.basepath AS basepath, " \
		"folders.foldername AS foldername, " \
		"files.filename AS filename, " \
		"files.extension AS extension " \
		"FROM " \
		"files, " \
		"folders, " \
		"video_metadata, " \
		"video_artworks " \
		"WHERE " \
		"video_metadata.video_artwork_id = video_artworks.video_artwork_id AND " \
		"video_metadata.fid = files.fid AND " \
		"files.folderid = folders.folderid ";

		mDb->queryCustomDB(query);
	}



	if (mCurrentRequestMethod == "pictures") {
	    QString memoryColumn;
        QString folder;
        if ( mCurrentMemory == "internal") {
            memoryColumn = "device memory";
            folder = "/accounts/1000/shared";
            mDb->initDB("/db/mmlibrary.db");
        }
        else {
            memoryColumn = "external memory";
            folder = "/sdcard/external_sd";
            mDb->initDB("/db/mmlibrary_SD.db");
        }
		QString query;
		query =
		"\nSELECT DISTINCT " \
		"\nphoto_metadata.actual_width AS width, " \
		"\nphoto_metadata.actual_height AS height, " \
		"\nphoto_metadata.orientation AS orientation, " \
		"\nphoto_metadata.shutter AS shutter, " \
		"\nphoto_metadata.aperture AS aperture, " \
		"\nphoto_metadata.focal_length AS focal_length, " \
		"\nphoto_metadata.iso AS iso, " \
		"\nphoto_metadata.date_original AS date_original, " \
		"\nphoto_metadata.date_timestamp AS date_timestamp, " \
		"\nphoto_metadata.latitude AS latitude, " \
		"\nphoto_metadata.longitude AS longitude, " \
		"\n'"+memoryColumn+"' AS memorytype, " \
        "\n'"+folder+"' || basepath || filename AS fullImagePath, " \

		"\nfolders.basepath AS basepath, " \
		"\nfolders.foldername AS foldername, " \
		"\nfiles.filename AS filename, " \
		"\nfiles.extension AS extension " \
		"\nFROM " \
		"\nfiles, " \
		"\nfolders, " \
		"\nphoto_metadata " \
		"\nWHERE " \
		"\nphoto_metadata.fid = files.fid AND " \
		"\nfiles.folderid = folders.folderid AND " \
		"\nfolders.basepath NOT LIKE '%protected%'";  // There are some protected system folders that should not be

		mDb->queryCustomDB(query);
	}

	if (mCurrentRequestMethod == "audio") {
	    QString memoryColumn;
	    QString folder;
	    if ( mCurrentMemory == "internal") {
            memoryColumn = "device memory";
            folder = "/accounts/1000/shared";
            mDb->initDB("/db/mmlibrary.db");
	    }
        else {
	        memoryColumn = "external memory";
	        folder = "/sdcard/external_sd";
            mDb->initDB("/db/mmlibrary_SD.db");
        }
        QString query;
        query =
        "SELECT DISTINCT " \
        "\naudio_metadata.title AS title, " \
        "\naudio_metadata.duration AS duration, " \
        "\naudio_metadata.bitrate AS bitrate, " \
        "\naudio_metadata.year AS year, " \
        "\naudio_metadata.samplerate AS samplerate, " \
        "\n'"+memoryColumn+"' AS memorytype, " \
        "\n'"+folder+"' || basepath || filename AS fullAudioPath, " \

        "\nfolders.basepath AS basepath, " \
        "\nfolders.foldername AS foldername, " \
        "\nfiles.filename AS filename, " \
        "\nfiles.extension AS extension " \
        "\nFROM " \
        "\nfiles, " \
        "\nfolders, " \
        "\naudio_metadata " \

        "\nWHERE " \
        "\naudio_metadata.fid = files.fid AND " \
        "\nfiles.folderid = folders.folderid";

        mDb->queryCustomDB(query);
	}

}

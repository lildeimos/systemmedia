/**
  \file dbase.cpp
  \author  Marco Bavagnoli <lil.deimos@gmail.com>
  \version 1.0

  \section LICENSE


  \section DESCRIPTION

  The dbase class provides the main functionalities to create or delete a sqlite database,
  manage tables (crete, delete) and manage records (add, delete, update).

  \version 1.0
  \date Nov 2011
 */

#include "dbase.h"

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#ifdef Q_OS_QNX
	#include <QtSql/QtSql>
//	#include <QtSql/QMYSQLDriver> ///// aaaaaahhhh merda!*@#]#@ non c'e' mysql.h
	#include <bps/netstatus.h>
#endif

#define OUT_DEBUG 0

#if OUT_DEBUG
#include <QDebug>
#endif


/**
  \fn dbase::dbase(QObject *parent) : QObject()
  \brief dbase constructor

  The constructor retrieve the path to store the data base file.
  \param parent The QObject parent pointer
 */
dbase::dbase(QObject *parent) :
    QObject(parent)
{
}
/**
  \fn dbase::dbase(QString sqliteFileName, QObject *parent) : QObject()
  \brief dbase constructor

  The constructor retrieve the path to store the data base file.
  \param parent The QObject parent pointer
  \param sqliteFileName file name of sqlite file without extension
*/
dbase::dbase(const QString sqliteFileName, QObject *parent) :
    QObject(parent)
{
	setPlatformDir();
	initDB(sqliteFileName);
}
/**
  \fn dbase::dbase(const QString host, const QString dbName, const QString userName, const QString password, QObject *parent) : QObject()
  \brief dbase constructor

  The constructor retrieve the data to open remote db
  \param params for remote MySql access
*/
dbase::dbase(const QString host, const QString dbName, const QString userName, const QString password, QObject *parent) :
    QObject(parent)
{
	initRemoteDB(host, dbName, userName, password);
}

/**
  \fn dbase::~dbase()
  \brief dbase des

  The destructor closes the data base.
  \param parent The QObject parent pointer
 */
dbase::~dbase()
{
    db.close();
}


void dbase::setPlatformDir()
{
    // Set directory to store db
    // For Android use /sdcard/+QCoreApplication::applicationName()
#if defined(Q_WS_X11) || defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5) || defined(Q_WS_SIMULATOR) || defined(MEEGO_EDITION_HARMATTAN)
    dataDir.setPath(QDesktopServices::storageLocation(QDesktopServices::DataLocation)+"/"+QCoreApplication::applicationName());
#elif defined(Q_OS_QNX)
    dataDir.setPath(QDir::currentPath() + "/data");
#else
    dataDir.setPath("/sdcard/"+QCoreApplication::applicationName());
#endif
}

void dbase::setDir(QString dir)
{
    dataDir.setPath(dir);
}

bool dbase::initRemoteDB(const QString host, const QString dbName, const QString userName, const QString password)
{
	if ( checkNetworkAvailability() != 1 ) {
		emit errorMessage(tr("Error: Network not available ?"));
		return false;
	}
	db = QSqlDatabase::addDatabase("QMYSQL");
	db.setHostName(host);
	db.setDatabaseName(dbName);
	db.setUserName(userName);
	db.setPassword(password);
	bool ok=db.open();

	if (ok && OUT_DEBUG)
		qDebug() <<  "dbase::initRemoteDB(): remote database opened";
	else
		qDebug() <<  "dbase::initRemoteDB() error: " << db.lastError().text();

	return ok;
}

/**
  \fn bool dbase::initDB(QString db_FileName)
  \brief Open sqlite dbase file.
         If the path doesn't exist it will be created, same for the dbase file
  \param db_FileName .sqlite file name without extension
  \return true if succesful
*/
bool dbase::initDB(QString db_FileName)
{
    if ( !dataDir.exists() ) {
        bool b=dataDir.mkpath(dataDir.absolutePath());
        if (b) {
            if (OUT_DEBUG) qDebug() << "Directory created:" << dataDir;
        }
        else {
            qDebug() << "Error creating directory" << dataDir;
            return false;
        }
    } else
    {
        if (OUT_DEBUG) qDebug() << "Directory already exists:" << dataDir;
    }


    db = QSqlDatabase::addDatabase("QSQLITE", db_FileName);
    dbFileName = db_FileName;
    if (!dbFileName.exists()) {
        qDebug() << "sqlite file doesn't exists yet: " << dbFileName.absoluteFilePath();
    }
    db.setDatabaseName(dbFileName.absoluteFilePath());

    bool ok=db.open();

    if (ok && OUT_DEBUG)
        qDebug() <<  "database opened:" << dbFileName.absoluteFilePath();
    else
        qDebug() <<  dbFileName.absoluteFilePath() << db.lastError().text();

    if (!ok) {
        emit errorMessage(tr("Error opening db file:")+dbFileName.baseName());
        emit dbOpenError();
    }

    return ok;
}

void dbase::closedb()
{
    db.close();
}

/**
  \fn bool dbase::purgeDB(QString db_FileName)
  \brief Delete .sqlite file9
  \param db_FileName .sqlite file name without extension
  \return true if succesful
*/
bool dbase::purgeDB(QString db_FileName)
{
    dbFileName = db_FileName;
    if (!dbFileName.exists()) {
        if (OUT_DEBUG) qDebug() << "Database file doesn't exist:" << dbFileName.absolutePath();
        return false;
    }
    QFile f(dbFileName.absoluteFilePath());
    bool b = f.remove();
    if (!b && OUT_DEBUG) qDebug() << "Cannot delete database file:" << dbFileName.absolutePath();
    if (b && OUT_DEBUG)  qDebug() << "Database file deleted:" << dbFileName.absolutePath();
    return b;
}



/**
  \fn bool dbase::createTable(QString table_name, QVariantMap fields, bool delete_table_if_exist)
  \brief Create a table in the database named 'main' with fields 'fields'.
          Usage example from QML:
          dbase.createTable("user",
          {
              "user":"TEXT",
              "passwd":"TEXT",
              "realname":"TEXT"
          },
          true
          )
  \param table_name table name
  \param fields pair of 'field name' and 'field type' --> http://www.sqlite.org/datatype3.html
  \param delete_table_if_exist if true delete the table, default is false
  \return true if succesful
*/
bool dbase::createTable(QString table_name, QVariantMap fields, bool delete_table_if_exist)
{
    QSqlQuery query(db);
    QString s;
    bool b;

    if (delete_table_if_exist) {
        b=query.exec("drop table if exists "+table_name);
        if (!b) qDebug() <<  "DELETE TABLE ERROR:" << query.lastError().text();
    }


    // Compose query to create the table
    s="CREATE TABLE '"+table_name+"' ('ID' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL UNIQUE ";
    QMapIterator<QString, QVariant> i(fields);
    while (i.hasNext()) {
        i.next();
        s += ", '" + i.key() + "' " + i.value().toString();
    }
    s += ")";
    if (OUT_DEBUG) qDebug() << "Query:" << s;


    b=query.exec(s);
    if (b  && OUT_DEBUG) qDebug() << table_name << "table created";
    if (!b) {
//    	errorMessage(tr("Error creating table:") + query.lastError().text());
    	qDebug() <<  "CREATE TABLE ERROR:" << query.lastError().text();
    }
    query.clear();
    query.finish();
    return b;
}


/**
  \fn bool dbase::addRecord(QString table_name, QVariantMap fields)
  \brief Add a recocord to table_name in the database named 'main' with fields 'fields'.
         Usage example from QML:
         dbase.addRecords("user",
                        {
                            "name":"John",
                            "surname":"Doe",
                            "city":"Smallville"
                         }
                        )
  \param table_name table name
  \param fields map of <name of field>,<value>
  \return true if succesful
*/
bool dbase::addRecord(QString table_name, QVariantMap fields)
{
    QSqlQuery query(db);
    QString s,s1,s2;


    // Compose query to add the records
    QMapIterator<QString, QVariant> i(fields);
    s1.clear(); s2.clear();
    while (i.hasNext()) {
        i.next();
        s1 += i.key() + ", ";
        s2 += ":" + i.key() + ", ";
    }
    // Remove last char from s1 and s2 because are ","
    s1 = s1.left(s1.size()-2);
    s2 = s2.left(s2.size()-2);

    s="INSERT INTO " + table_name + " (" + s1 + ") " + "VALUES (" + s2 + ")";
    query.prepare(s);
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    // Bind values
    i.toFront();
    while (i.hasNext()) {
        i.next();
        query.bindValue(":"+i.key(),  i.value().toString());
    }

    if (!query.exec()) {
        if (OUT_DEBUG) qDebug() << "Failed to add recod:" << query.lastError().text();
        return false;
    }
    if (OUT_DEBUG) qDebug() << "Record(s) added";

    return true;
}


/**
  \fn bool dbase::deleteRecord(QString table_name, QString ID)
  \brief Delete the recocord from table_name with ID
  \param table_name table name
  \param ID of the record to delete
  \return true if succesful
*/
bool dbase::deleteRecord(QString table_name, QString ID)
{
    QSqlQuery query(db);

    query.prepare("DELETE FROM "+table_name+
                  " WHERE ID="+"'"+ID+"'");

    if (!query.exec()) {
        if (OUT_DEBUG) qDebug() << "Failed to delete recod:" <<  query.lastError().text();
        return false;
    }
    if (OUT_DEBUG) qDebug() << "Record deleted";

    return true;
}


/**
  \fn bool dbase::updateRecord_byID(QString table_name, QString ID, QVariantMap fields)
  \brief Update the recocord from table_name with ID with new values from fields
  \param table_name table name
  \param ID of the record to update
  \param fields map of <name of field>,<value>
  \return true if succesful
*/
bool dbase::updateRecord_byID(QString table_name, QString ID, QVariantMap fields)
{
    QSqlQuery query(db);
    QString s,s1;


    // Compose query to add the records
    QMapIterator<QString, QVariant> i(fields);
    s1.clear();
    while (i.hasNext()) {
        i.next();
        s1 += i.key() + "=:" + i.key() + ", ";
    }
    // Remove last 2 chars from s1 because are ", "
    s1 = s1.left(s1.size()-2);


    s="UPDATE " + table_name + " SET " + s1 + " WHERE ID='"+ID+"'";
    query.prepare(s);
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    // Bind values
    i.toFront();
    while (i.hasNext()) {
        i.next();
        query.bindValue(":"+i.key(),  i.value().toString());
    }

    if (!query.exec()) {
        if (OUT_DEBUG) qDebug() << "Failed to update recod:" << query.lastError().text();
        return false;
    }
    if (OUT_DEBUG) qDebug() << "Record(s) updated";

    return true;
}


/**
  \fn bool dbase::updateRecord_withWhere(QString table_name, QString whereParam, QVariantMap fields)
  \brief Update the recocord from table_name using a whereParam with new values from fields
  \param table_name table name
  \param ID of the record to update
  \param fields map of <name of field>,<value>
  \return true if succesful
*/
bool dbase::updateRecord_withWhere(QString table_name, QString whereParam, QVariantMap fields)
{
    QSqlQuery query(db);
    QString s,s1;


    // Compose query to add the records
    QMapIterator<QString, QVariant> i(fields);
    s1.clear();
    while (i.hasNext()) {
        i.next();
        s1 += i.key() + "=:" + i.value().toString() + ", ";
    }
    // Remove last 2 chars from s1 because are ", "
    s1 = s1.left(s1.size()-2);


    s="UPDATE " + table_name + " SET " + s1 + " WHERE "+whereParam+" ";
    query.prepare(s);
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    // Bind values
    i.toFront();
    while (i.hasNext()) {
        i.next();
        query.bindValue(":"+i.key(),  i.value().toString());
    }

    if (!query.exec()) {
        if (OUT_DEBUG) qDebug() << "Failed to update recod:" << query.lastError().text();
        return false;
    }
    if (OUT_DEBUG) qDebug() << "Record(s) updated";

    return true;
}


/**
  \fn QStringList dbase::queryDB(QString table_name, QString whereParam, QString orderByField, QString ascDesc)
  \brief Query the dbase within the table table_name with the WHERE clausole 'whereParam'.
         Optionally give a field name to order by and a method to.
         A signal is emitted for every result containing the whole record in a QVarianMap constant
         QML example for the returning value:
         // http://doc.qt.nokia.com/4.7-snapshot/qtbinding.html#javascript-arrays-and-objects

         var anArray=dbase.queryDB("user","user LIKE '%Marco%'");
         for (var i=0; i<anArray.length; i++)
                      console.log("Array item:", anArray[i])

         QML example to catch the signal and manage the data:
             Connections {
                  target: dbase
                  onGotRecord: {
                      for (var prop in rec) {
                                   console.log("Object item:", prop, "=", rec[prop])
                               }
                      console.log( rec.user, rec.passwd, rec.prova);
                  }
              }

         whereParam examples:
             user LIKE '%John%' AND (city='New York' OR city='Washington')


  \param table_name table name ( if more then one they should be separated by comma )
  \param whereParam string with the WHERE clausole
  \param orderByField optional field name to order by
  \param ascDesc method of sorting. Could be ASC for ascending, DESC for descending
  \return A list of IDs matching the query
*/
void dbase::queryDB(QString table_name, QString whereParam, QString orderByField, QString ascDesc)
{
    QSqlQuery query(db);
    QString s;

    // Compose query
    s = "SELECT * FROM "+table_name+" WHERE " + whereParam;
    if (!orderByField.isEmpty())
        s += " ORDER BY "+orderByField+" "+ascDesc;
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    if (!query.exec(s)) {
        if (OUT_DEBUG) qDebug() << "Failed to query dbase:" << query.lastError().text();
        return;
    }

    if (OUT_DEBUG) qDebug() << "Number of columns queryDB(): " << query.record().count();
    while (query.next())
    {
        QVariantMap rec;
        for (int i=0; i<query.record().count(); i++)  {
            rec.insert( query.record().fieldName(i) , query.record().value(i).toString() );
        }
        if (!rec.isEmpty()) emit gotRecord(rec);
    }
}

// Do a query with expected results in return
void dbase::queryCustomDB(QString queryStr)
{
    QSqlQuery query(db);

    if (OUT_DEBUG) qDebug() << "Query:" << queryStr;

    if (!query.exec(queryStr)) {
        if (OUT_DEBUG) qDebug() << "Failed to query dbase:" << query.lastError().text();
        return;
    }

    if (OUT_DEBUG) qDebug() << "Number of columns queryCustomDB(): " << query.record().count();
    while (query.next())
    {
        QVariantMap rec;
        for (int i=0; i<query.record().count(); i++)  {
            rec.insert( query.record().fieldName(i) , query.record().value(i).toString() );
        }
        if (!rec.isEmpty()) emit gotRecord(rec);
    }
    query.clear();
	query.finish();
    emit gotResultsFinished();
}

// Do a query with expected results in return
void dbase::queryCustomDB_n(QString queryStr, int numberOfMaxResults)
{
    QSqlQuery query(db);


    if (OUT_DEBUG)
        qDebug() << "Query:" << queryStr;

    if (!query.exec(queryStr)) {
//        if (OUT_DEBUG)
            qDebug() << "Failed to query dbase:" << query.lastError().text();
        return;
    }

    if (OUT_DEBUG)
        qDebug() << "Number of columns queryCustomDB(): " << query.record().count() << query.numRowsAffected() << query.size();
    QVariantMap rec;
    QVariantList list;
    int n=0, tot=0;
    while (query.next())
    {
        for (int i=0; i<query.record().count(); i++)  {
            rec.insert( query.record().fieldName(i) , query.record().value(i).toString() );
        }
        if (!rec.isEmpty()) {
            tot++;
            list << rec;
            n++;
            if (n>numberOfMaxResults) {
                emit gotRecords(list);
                list.clear();
                n=0;
            }
            rec.clear();
        }
    }
    if (n>0) emit gotRecords(list);
    if (OUT_DEBUG) qDebug() << "queryCustomDB() FINISHED count=" << tot;

    query.clear();
    query.finish();
    emit gotResultsFinished();
}

// do a passive custom query like UPDATE or DELETE without having rows returned
bool dbase::queryPassiveCustomDB(QString queryStr)
{
    QSqlQuery query(db);

    if (OUT_DEBUG) qDebug() << "Query:" << queryStr;

    if (!query.exec(queryStr)) {
//        if (OUT_DEBUG)
            qDebug() << "Failed to query dbase:" << query.lastError().text();
        return false;
    }

//    if (OUT_DEBUG)
        qDebug() << "Number of columns queryPassiveCustomDB(): " << query.record().count();
    query.clear();
	query.finish();
    emit gotResultsPassiveFinished();
    return true;
}

bool dbase::queryFirstDB(QString table_name, QString whereParam)
{
    QSqlQuery query(db);
    QString s;

    // Compose query
    s = "SELECT * FROM "+table_name+" WHERE " + whereParam +" COLLATE NOCASE";
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    if (!query.exec(s)) {
//        if (OUT_DEBUG)
            qDebug() << "Failed to query dbase:" << query.lastError().text();
        return false;
    }

    if (query.first())
        return true;
    return false;
}

int dbase::countTableRows(QString table_name)
{
    QSqlQuery query(db);
    QString s;

    // Compose query
    s = "SELECT COUNT(*) AS count FROM " + table_name;
    if (OUT_DEBUG) qDebug() << "Query:" << s;

    if (!query.exec(s)) {
        if (OUT_DEBUG) qDebug() << "Failed to query dbase:" << query.lastError().text();
        return false;
    }

    if (query.first())
        return query.value(0).toInt();
    return 0;
}







//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////
// functions to check network state and kind

// Return:
//	-1 for unknow
//	0  not available
//	1  available
// TODO: implement other OS
int dbase::checkNetworkAvailability()
{
	int ret = -1;
#ifdef Q_OS_LINUX
    ret = 1;
#endif
#ifdef Q_OS_QNX
	netstatus_interface_details_t *details;
	netstatus_get_interface_details(NULL, &details);
	int c = netstatus_interface_get_num_ip_addresses(details);
	const char *  wifi = netstatus_interface_get_link_address(details);
	bool netAvail = false;
	netstatus_get_availability(&netAvail); // https://developer.blackberry.com/native/reference/bb10/com.qnx.doc.bps.lib_ref/com.qnx.doc.bps.lib_ref/topic/netstatus_get_availability.html
	if (netAvail) ret = 1;
	else ret = 0;
	qDebug() << "MBNetwork::checkNetworkAvailability()" << netAvail << c << wifi;
#endif
	return ret;
}

// Return:
//	-1 for unknow
//	0  not available
//	1  cellular, usb, blutooth
//  2  wifi, wired, vpn etc
// TODO: implement other OS
int dbase::networkType()
{
	int ret = -1;
#ifdef Q_OS_LINUX
    ret = 1;
#endif
#ifdef Q_OS_QNX
	netstatus_interface_type_t type;
	netstatus_interface_details_t *details;
	netstatus_get_interface_details(NULL, &details);
	type = netstatus_interface_get_type(details);
	bool netAvail = false;
	netstatus_get_availability(&netAvail); // https://developer.blackberry.com/native/reference/bb10/com.qnx.doc.bps.lib_ref/com.qnx.doc.bps.lib_ref/topic/netstatus_get_availability.html
	qDebug() << "MBNetwork::networkType()" << netAvail << type;
	if (!netAvail) return 0;
	if (type == NETSTATUS_INTERFACE_TYPE_UNKNOWN) return -1;
	if (type == NETSTATUS_INTERFACE_TYPE_CELLULAR ||
		type == NETSTATUS_INTERFACE_TYPE_BLUETOOTH_DUN ||
		type == NETSTATUS_INTERFACE_TYPE_USB) return 1;
	return 2;

#endif
	return ret;
}
















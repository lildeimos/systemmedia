/**
  \file dbase.h
  \author  Marco Bavagnoli <lil.deimos@gmail.com>
  \version 1.0

  \section LICENSE


  \section DESCRIPTION

  The dbase class provides the main functionalities to create or delete a sqlite database,
  manage tables (crete, delete) and manage records (add, delete, update).

  \version 1.0
  \date Nov 2011
 */

#ifndef DBASE_H
#define DBASE_H

#include <QObject>
#include <QDir>
#include <QUrl>
#include <QMap>
#include <QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QtSql>
#include <QFileInfo>
#ifndef Q_OS_QNX
#include <QDesktopServices>
#endif

class dbase : public QObject
{
    Q_OBJECT
public:
    explicit dbase(QObject *parent = 0);
    dbase(const QString sqliteFileName, QObject *parent);
    dbase(const QString host, const QString dbName, const QString userName, const QString password, QObject *parent);
    ~dbase();

    Q_INVOKABLE bool initRemoteDB(const QString host, const QString dbName, const QString userName, const QString password);
    Q_INVOKABLE bool initDB(QString db_FileName);
    Q_INVOKABLE void closedb();
    Q_INVOKABLE void setPlatformDir();
    Q_INVOKABLE void setDir(QString dir);
    Q_INVOKABLE bool purgeDB(QString db_FileName);
    Q_INVOKABLE bool createTable(QString table_name, QVariantMap fields, bool delete_table_if_exist=false);
    Q_INVOKABLE bool addRecord(QString table_name, QVariantMap fields);
    Q_INVOKABLE bool deleteRecord(QString table_name, QString ID);
    Q_INVOKABLE bool updateRecord_byID(QString table_name, QString ID, QVariantMap fields);
    Q_INVOKABLE bool updateRecord_withWhere(QString table_name, QString whereParam, QVariantMap fields);
    Q_INVOKABLE void queryDB(QString table_name, QString whereParam, QString orderByField="", QString ascDesc="ASC");
    Q_INVOKABLE void queryCustomDB(QString queryStr);
    Q_INVOKABLE void queryCustomDB_n(QString queryStr, int numberOfMaxResults);
    Q_INVOKABLE bool queryPassiveCustomDB(QString queryStr);
    Q_INVOKABLE bool queryFirstDB(QString table_name, QString whereParam);
    Q_INVOKABLE int countTableRows(QString table_name);

    static int checkNetworkAvailability();
    static int networkType();

private:
    QDir dataDir;
    QSqlDatabase db;
    QFileInfo dbFileName;


Q_SIGNALS:
    void gotRecord( const QVariantMap rec );
    void gotRecords(QVariantList list);
    void gotResultsFinished();
    void gotResultsPassiveFinished();
	void errorMessage(const QString message);
	void dbOpenError();

public slots:
};

#endif // DBASE_H

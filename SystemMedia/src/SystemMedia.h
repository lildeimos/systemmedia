/*
 * SystemMedia.h
 *
 *  Created on: 6 Oct 2015
 *      Author: Marco Bavagnoli
 *      Copyright (c) 2015 Marco Bavagnoli <lil.deimos@gmail.com>
 */

#ifndef SYSTEMMEDIA_H_
#define SYSTEMMEDIA_H_

#include "dbase.h"

#include <qobject.h>
#include <QVariantList>
#include <QVariantMap>
#include <QThread>
#include <QDir>

class SystemMedia: public QThread {
	Q_OBJECT

public:
	SystemMedia();
	virtual ~SystemMedia();

	Q_INVOKABLE void getVideos();
	Q_INVOKABLE void getPictures();
	Q_INVOKABLE void getAudio();
	Q_INVOKABLE void deleteFile(QString absoluteFileName);

private:
	dbase *mDb;
	QString mCurrentRequestMethod;
	QString mCurrentMemory;
	int mCounter;

protected:
	void run();

private slots:
	void onGotQueryResult(const QVariantMap rec);
	void onGotResultsFinished();
	void onDBOpenError();

Q_SIGNALS:
	void gotThumbVideo(QVariantList list);
	void gotThumbImage(QVariantList list);
	void gotThumbAudio(QVariantList list);
	void finishedVideos();
	void finishedImages();
	void finishedAudio();
	void noVideosFound();
	void noImagesFound();
	void noAudioFound();
};

#endif /* SYSTEMMEDIA_H_ */
